package com.ccmbenchmark.pharmap.util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ccmbenchmark.pharmap.MainActivity;
import com.ccmbenchmark.pharmap.SearchResultsActivity;
import com.ccmbenchmark.pharmap.constantes.Constantes;
import com.example.pharmap.R;

public class ProductResultsJSONAdapter extends BaseAdapter implements ListAdapter {

	private final Activity activity;
	private final JSONArray jsonArray;
	public ProductResultsJSONAdapter(Activity activity, JSONArray jsonArray) {
		assert activity != null;
		assert jsonArray != null;

		this.jsonArray = jsonArray;
		this.activity = activity;
	}


	@Override public int getCount() {
		return jsonArray.length();
	}

	@Override public JSONObject getItem(int position) {
		return jsonArray.optJSONObject(position);
	}

	@Override public long getItemId(int position) {
		JSONObject jsonObject = getItem(position);

		return jsonObject.optLong("id");
	}

	@Override public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderProduct viewHolder;
		if (convertView == null){
			convertView = activity.getLayoutInflater().inflate(R.layout.product_row, null);
			viewHolder = new ViewHolderProduct();
			viewHolder.label = (TextView)convertView.findViewById(R.id.product_label);
			viewHolder.quantity = (TextView)convertView.findViewById(R.id.product_quantity);
			viewHolder.form = (TextView)convertView.findViewById(R.id.product_form);
			convertView.setTag(viewHolder);
		}
		else{
			viewHolder = (ViewHolderProduct) convertView.getTag();
		}
		
		final JSONObject jsonObject = getItem(position);  
		try {
			viewHolder.label.setText(jsonObject.getString("label"));
			//TODO refaire une listview pour les différentes formes du médicament
			viewHolder.quantity.setText(jsonObject.getJSONArray("form").getJSONObject(0).getString("quantity"));
			viewHolder.form.setText(jsonObject.getJSONArray("form").getJSONObject(0).getString("id"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//Ecouteurs sur les items
		

		return convertView;
	}
	private class ViewHolderProduct{
		public TextView label;
		public TextView quantity ;
		public TextView form ;
	}
}
