package com.ccmbenchmark.pharmap.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.pharmap.R;

public class PharmacyResultsJSONAdapter extends BaseAdapter implements ListAdapter {

	private final Activity activity;
	private final JSONArray jsonArray;
	
	public  PharmacyResultsJSONAdapter(Activity activity, JSONArray jsonArray) {
		assert activity != null;
		assert jsonArray != null;

		this.jsonArray = jsonArray;
		this.activity = activity;
	}


	@Override public int getCount() {
		return jsonArray.length();
	}

	@Override public JSONObject getItem(int position) {
		return jsonArray.optJSONObject(position);
	}

	@Override public long getItemId(int position) {
		JSONObject jsonObject = getItem(position);

		return jsonObject.optLong("id");
	}

	@Override public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderProduct viewHolder;
		if (convertView == null){
			convertView = activity.getLayoutInflater().inflate(R.layout.product_row, null);
			viewHolder = new ViewHolderProduct();
			viewHolder.label = (TextView)convertView.findViewById(R.id.label_pharmacy);
			viewHolder.address = (TextView)convertView.findViewById(R.id.address_pharmacy);
			viewHolder.distance = (TextView)convertView.findViewById(R.id.dist_pharmacy);
			
			convertView.setTag(viewHolder);
		}
		else{
			viewHolder = (ViewHolderProduct) convertView.getTag();
		}

		final JSONObject jsonObject = getItem(position);  
		try {
			viewHolder.label.setText(jsonObject.getString("label"));
			viewHolder.address.setText(jsonObject.getString("adress"));
			viewHolder.distance.setText("0,4 km");
			//TODO faire la différence entre notre geo et les param long latt
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//Ecouteurs sur les items


		return convertView;
	}
	private class ViewHolderProduct{
		public TextView label;
		public TextView address ;
		public TextView distance ;
	}
}
