package com.ccmbenchmark.pharmap.util;

import java.util.UUID;

import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager.NameNotFoundException;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class Utils {
	/**
	 * Return application version
	 * @param cw the context wrapper
	 * @return String application version
	 */
	public static String getApplicationVersion(ContextWrapper cw){
		try {
			return cw.getPackageManager().getPackageInfo(cw.getApplicationContext().getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Return application package name
	 * @param cw the context wrapper
	 * @return String application package name
	 */
	public static String getApplicationPackageName(ContextWrapper cw){
		try {
			return cw.getPackageManager().getPackageInfo(cw.getApplicationContext().getPackageName(), 0).packageName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Return screen density of the device
	 * @param cw a context wrapper
	 * @return float sceen density
	 */
	public static float getDensity(ContextWrapper cw){
		return cw.getResources().getDisplayMetrics().density;
	}


/**
	 * Return a unique terminal identifiant 
	 * @param cw a context wrapper
	 * @param cr a content resolver
	 * @return the terminal identifiant
	 */
	// http://www.experience-developpement.fr/android-identifiant-unique/
	public static String getIdentifiantTerminal(ContextWrapper cw, ContentResolver cr){
		
		final TelephonyManager tm = (TelephonyManager)cw.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
		
		String android_id = Secure.getString(cr,Secure.ANDROID_ID);
		//numéro IMEI
		String device_id = tm.getDeviceId();
		
		//UUID deviceUUID = new UUID(android_id.hashCode(),((long)device_id.hashCode() << 32));
		if(android_id == null) android_id = "123456789";
		if(device_id == null) device_id = "123456789";
		UUID deviceUUID = new UUID(android_id.hashCode(),(long)device_id.hashCode());
		return deviceUUID.toString();
	}
}
