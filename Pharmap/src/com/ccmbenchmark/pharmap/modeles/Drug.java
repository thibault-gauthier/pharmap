package com.ccmbenchmark.pharmap.modeles;

import org.json.JSONException;
import org.json.JSONObject;

public class Drug extends DecodeJson{
	
	


	private String form;
	private String quantitity;
	
	public Drug(JSONObject object) throws JSONException {
		super(object);
		this.form = object.getString("id");
		this.quantitity = object.getString("quantity");
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getQuantitity() {
		return quantitity;
	}

	public void setQuantitity(String quantitity) {
		this.quantitity = quantitity;
	}
	
	
}
