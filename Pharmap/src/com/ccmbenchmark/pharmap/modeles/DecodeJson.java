package com.ccmbenchmark.pharmap.modeles;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class DecodeJson {
	private JSONObject object;
	public DecodeJson(JSONObject object)  throws JSONException{
		this.object = object;
		
	}
	public JSONObject getObject() {
		return object;
	}
	public void setObject(JSONObject object) {
		this.object = object;
	}
	
	
}
