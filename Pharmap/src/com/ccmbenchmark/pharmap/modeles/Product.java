package com.ccmbenchmark.pharmap.modeles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Product extends DecodeJson{

	private int id; //id du produit
	private String label; //nom du produit
	private List<Drug> form; //Forme du produit + quantité disponible
	
	public Product(JSONObject object) throws JSONException {
		super(object);
		this.id = object.getInt("id");
		this.label = object.getString("label");
		this.form = new ArrayList<Drug>();
		JSONArray arrayForm = object.getJSONArray("form");
		for(int i = 0; i < arrayForm.length(); i++){
			this.form.add(new Drug(arrayForm.getJSONObject(i)));
		}
	}

	public int getId() {
		return id;
	}


	public String getLabel() {
		return label;
	}

	public List<Drug> getForm() {
		return form;
	}

	public void setForm(List<Drug> form) {
		this.form = form;
	}
	
	



}
