package com.ccmbenchmark.pharmap;

import com.ccmbenchmark.pharmap.constantes.Constantes;
import com.example.pharmap.R;

import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.Button;
/***
 * 
 * @description page d'accueil
 *
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
/*
		//Evenement sur la barre de recherche
		final EditText searchbar = (EditText) this.findViewById(R.id.searchbar);
		searchbar.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(searchbar)
		{
			@Override
			public boolean onDrawableClick()
			{
				//On change d'activité lorsque l'utilisateur clique sur la recherche et on envoie le paramètre dans EXTRA
				Intent intent = new Intent(MainActivity.this, ResultSearchActivity.class);
				intent.putExtra(EXTRA_SEARCH, searchbar.getText().toString());
				startActivity(intent);
				return true;
			}
		} );*/
	}
	//Permet de crée notre menu en lisant le fichier xml
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		//R.menu.menu est l'id de notre menu
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	//Gestion des écouteurs sur le menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu_accueil:
	        	setContentView(R.layout.activity_main);
	            return true;
	        case R.id.menu_recherche:
	        	//On charge la recherche
	        	Intent intent = new Intent(MainActivity.this, SearchResultsActivity.class);
	        	//On lui transmet le parametre de recherche
				intent.putExtra(Constantes.PARAM_KEY, "doliprane");
				//Et on démarre l'activité
				startActivity(intent);
				finish();
				return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
