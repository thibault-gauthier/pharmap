package com.ccmbenchmark.pharmap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ccmbenchmark.pharmap.constantes.Constantes;
import com.ccmbenchmark.pharmap.util.PharmacyResultsJSONAdapter;
import com.ccmbenchmark.pharmap.util.ProductResultsJSONAdapter;
import com.ccmbenchmark.pharmap.util.Utils;
import com.ccmbenchmark.pharmap.webservices.Webservice;
import com.example.pharmap.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SearchResultsActivity extends Activity{
	//TAG pour les logs
	private static final String TAG = "SearchResultsActivity";
	//Terme de la recherche, ce que l'utilisateur veut rechercher
	private String searchTerm;
	private JSONArray products;
	private JSONArray pharmacies;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//On charge le layout de la page
		setContentView(R.layout.activity_search_results);
		//On récupére le paramétre de recherche
		Bundle b = getIntent().getExtras();
		searchTerm =  b.getString(Constantes.PARAM_KEY);
		//On récupére le résultat grace au webservice et on affiche le tout
		getAndShowJSONArray(searchTerm);

	}

	public void getAndShowJSONArray(String searchTerm){

		RequestParams params = new RequestParams();
		params.put("f_objrequetehttp_systeme_identifiant", Utils.getIdentifiantTerminal(this, getContentResolver()));
		params.put("f_application_identifiant", Utils.getApplicationPackageName(this));
		params.put("f_application_version", Utils.getApplicationVersion(this));
		params.put("search", searchTerm);
		params.put("search_in", ""); //on cherche pharmacies + produits

		Webservice.get("search.php", params, new JsonHttpResponseHandler() {
			private Toast toastLoading = Toast.makeText(getApplicationContext(), "Chargement des résultats", Toast.LENGTH_LONG);
			@Override
			public void onStart() {
				Log.v(TAG,"le webservice est dans onStart");
				final TextView text_search_results_products = (TextView) findViewById(R.id.text_search_results_products);
				//On met le texte à jour avec le nombre de produits trouvé
				text_search_results_products.setText("Chargement");
				toastLoading.show();
			}

			@Override
			public void onSuccess(JSONObject result) {
				Log.v(TAG,"le webservice est dans onSuccess");
				try {
					products = result.getJSONArray("product");
					pharmacies = result.getJSONArray("pharmacy");
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure( Throwable error)
			{
				setContentView(R.layout.product_no_results);
				Log.v(TAG,"le webservice est dans onFailure");
			}

			@Override
			public void onFinish() {
				toastLoading.cancel();
				Log.v(TAG,"le webservice est dans onFInish");
				if(products != null){
					final ListView products_list = (ListView) findViewById(R.id.products_list);	
					//On affiche le contenu dans notre listview
					Log.v(TAG, "Le webservice nous renvoie : "+products);
					ListAdapter adapter = new ProductResultsJSONAdapter(SearchResultsActivity.this,products);
					products_list.setAdapter(adapter);
					//Ecouteurs sur les elements de la liste - on envoie l'ID du produit en param
					products_list.setOnItemClickListener(new OnItemClickListener(){
						@Override
						public void onItemClick(AdapterView<?> adp,
								View v, int pos, long arg3) {
							Intent intent = new Intent(SearchResultsActivity.this, MainActivity.class);
				        	//On lui transmet le parametre de recherche
							long id = adp.getItemIdAtPosition(pos);
							intent.putExtra(Constantes.PARAM_KEY, id);
							//Et on démarre l'activité
							startActivity(intent);
							finish();

						}
					});    

					final TextView text_search_results_products = (TextView) findViewById(R.id.text_search_results_products);
					//On met le texte à jour avec le nombre de produits trouvé
					text_search_results_products.setText("Produits ("+adapter.getCount()+")");
					View separator = (View) findViewById(R.id.separator_products);
					separator.setVisibility(View.VISIBLE);
				}
				
				//Pharmacies
				if(pharmacies != null){
					final ListView pharmacies_list = (ListView) findViewById(R.id.pharmacies_list);	
					ListAdapter adapter = new PharmacyResultsJSONAdapter(SearchResultsActivity.this,pharmacies);
					pharmacies_list.setAdapter(adapter);
					//Ecouteurs sur les elements de la liste - on envoie l'ID du produit en param
					pharmacies_list.setOnItemClickListener(new OnItemClickListener(){
						@Override
						public void onItemClick(AdapterView<?> adp,
								View v, int pos, long arg3) {
							Intent intent = new Intent(SearchResultsActivity.this, MainActivity.class);
				        	//On lui transmet le parametre de recherche
							long id = adp.getItemIdAtPosition(pos);
							intent.putExtra(Constantes.PARAM_KEY, id);
							//Et on démarre l'activité
							startActivity(intent);
							finish();

						}
					});    

					final TextView text_search_results_pharmacies = (TextView) findViewById(R.id.text_search_results_pharmacy);
					//On met le texte à jour avec le nombre de produits trouvé
					text_search_results_pharmacies.setText("Pharmacies ("+adapter.getCount()+")");
					View separator = (View) findViewById(R.id.separator_pharmacy);
					separator.setVisibility(View.VISIBLE);
				}

			}

		});
	}




}
