package com.ccmbenchmark.pharmap;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.example.pharmap.R;
import com.sentaca.android.accordion.AccordionWidgetDemoActivity;


public class ProductPageActivity extends FragmentActivity{

	TabHost tabHost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_page);
		
		AccordionWidgetDemoActivity acc = new AccordionWidgetDemoActivity();

		//On définit un TabHost qui va accueillir nos deux onglets
		tabHost=(TabHost)findViewById(R.id.product_page_tabs);
		tabHost.setup();

		// On définit le premier onglet qui contient les informations d'un produit
		TabSpec spec1=tabHost.newTabSpec("Infos");
		spec1.setContent(com.example.pharmap.R.id.example_get_by_id);
		spec1.setIndicator("Infos");

		// On définit le second onglet qui contient les prix du produit
		TabSpec spec2=tabHost.newTabSpec("Prix");
		spec2.setIndicator("Prix");
		spec2.setContent(com.example.pharmap.R.id.example_get_by_id);
		
		// On ajout nos deux onglets au TabHost qui les accueille
		tabHost.addTab(spec1);
		tabHost.addTab(spec2); 
	}
}
